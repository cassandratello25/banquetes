<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculums extends Model
{
    protected $fillable=['nombre','apellido','edad','cargo','foto','email'];
}
