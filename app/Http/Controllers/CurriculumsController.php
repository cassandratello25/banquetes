<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CurriculumsRequest;
use App\Curriculums;

class CurriculumsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curriculums= Curriculums::all();
        return view('lista_curriculums', compact('curriculums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('curriculums');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurriculumsRequest $datos)
    {
        $curriculums=new Curriculums();
        $curriculums->nombre=$datos->nombre;
        $curriculums->apellido=$datos->apellido;
        $curriculums->edad=$datos->edad;
        $curriculums->cargo=$datos->cargo;
        $datos->imagen->store(config('misimg.path'), 'public');
        $curriculums->foto='foto/' .$datos->imagen->hashName();
        $curriculums->email=$datos->email;
        $curriculums->save();
        return  redirect()->route('curriculums.index')->with('mensaje','El curriculum fue agregado correctamente');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Curriculums $curriculum)
    {
        return view('detalle_curriculum',compact('curriculum'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Curriculums $curriculum)
    {
        //
        return view('editar', compact('curriculum'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurriculumsRequest $datos, Curriculums $curriculum)
    {
        $curriculum->update($datos->all()); 
        return redirect()->route('curriculums.index')->with('mensaje','El curriculum ha sido editado correctamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curriculums $curriculum)
    {
        //
        $curriculum->delete();
        return back()->with('mensaje','El curriculum ha sido eliminado correctamente');
    }
}
