<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurriculumsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'bail|required|between:4, 100',
            'apellido'=>'bail|required|between:4,100',
            'edad'=>'numeric',
            'cargo'=>'bail|required|between:4, 100',
            'imagen'=>'bail|required|image',
            'email'=>'bail|required|email'
            
        ];
    }
}
