<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservacionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'bail|required|between:4, 100',
            'email'=>'bail|required|email',
            'numero'=>'integer',
            'fecha'=>'bail|required|date',
            'hora'=>'bail|required|time',
            'lugares'=>'bail|required|between:4, 100',
            'banquete'=>'bail|required|between:4, 100'
            
        ];
    }
}