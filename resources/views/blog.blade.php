<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Blog - Banquetes Gourmet</title>
      <!-- Google Fonts -->
      <link rel='stylesheet' id='caverta-font-css'  href='http://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700%7CLora:400,700,400i,700i' type='text/css' media='all' />
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' id='bootstrap-css'  href='assets/css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
      <!-- Font Awesome Icons CSS -->
      <link rel='stylesheet' id='font-awesome'  href='assets/css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
      <!-- Owl Carousel -->
      <link rel='stylesheet' id='owl-carousel'  href='assets/js/owl-carousel/owl.carousel.min.css' type='text/css' media='all' />
      <!-- Main CSS File -->
      <link rel='stylesheet' id='caverta-style-css'  href='style.css' type='text/css' media='all' />
      <!-- favicons -->
      <link rel="icon" href="assets/images/icons/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" href="assets/images/icons/favicon-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="assets/images/icons/favicon-180x180.png" />
   </head>
   <body class="home">
      <div class="menu-mask"></div>
      <!-- MOBILE MENU HOLDER -->
      <div class="mobile-menu-holder">
         <div class="modal-menu-container">
            <div class="exit-mobile">
               <span class="icon-bar1"></span>
               <span class="icon-bar2"></span>
            </div>
            <!-- MOBILE MENU -->
            <ul class="menu-mobile">
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
						
					</li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/blog'); ?>">Blog</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
						
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
						
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
						
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
						
					</li>
            </ul>
            <!-- /MOBILE MENU -->
         </div>
         <!-- modal-menu-container -->
         <div class="menu-contact">
			<div class="mobile-btn"> <a href="#" class="view-more">Reserva</a></div>
            <ul class="mobile-contact">
               <li class="mobile-address">calle 20<br />
                  Cancún, Quintana Roo CP: 77535
               </li>
               <li class="mobile-phone">9981801087</li>
               <li class="mobile-email">cassandratello25@gmail.com</li>
            </ul>
            <ul class="social-media">
               <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
               <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
               <li><a class="social-gplus" href="#" target="_blank"><i class="fab fa-google-plus"></i></a></li>
               <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
               <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
               <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
            </ul>
         </div>
         <!-- /menu-contact-->
      </div>
      <!-- /MOBILE MENU HOLDER -->
      <!-- HEADER -->
      <header id="header-1" class="headerHolder header-1">
         <div class="nav-button-holder">
            <button type="button" class="nav-button">
            <span class="icon-bar"></span>
            </button>
         </div>
         <!-- /nav-button-holder-->
         <!-- LOGO -->
         <div class="logo logo-1"><a href="index.html"><img class="img-fluid" src="assets/images/caverta-logo.png" alt="Caverta" /></a></div>
         <!-- MENU -->
         <nav class="nav-holder nav-holder-1">
            <ul class="menu-nav menu-nav-1">
                <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
						
                </li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/contenido'); ?>">Blog</a>
						
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
						
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
					
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					
					</li>
               
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					
					</li>
            </ul>
         </nav>
         <!-- /MENU --> 
         <div class="btn-header">
            <p><a href="<?= url('/reservaciones'); ?>" class="view-more">Reservar</a></p>
         </div>
      </header>
      <!-- /HEADER -->
      <!-- TOP IMAGE -->
      <section class="topSingleBkg topPageBkg">
         <div class="item-img top-blog-list"></div>
         <div class="inner-desc">
            <h1 class="post-title single-post-title">Blog</h1>
            <span class="post-subtitle"> Latest News</span>
         </div>
      </section>
      <!-- /TOP IMAGE --> 
      <!-- WRAP CONTENT -->
      <div class="container blog-holder">
         <div class="row">
            <div class="col-md-9 posts-holder">
               <section id="wrap-content" class="blog-1col-list-left">
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image1"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 14, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Food</a> <a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Chef New Summer Dish</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image2"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 13, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Food</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Realm of the Spirit Whisky</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image3"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 12, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Restaurant Signature Dinner</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image4"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 11, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Desserts</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Chocolate Muffins Gift</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image5"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 10, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Desserts</a> <a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Royal Yacht Club Cocktail</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image6"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 9, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Italian</a> <a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Chicken milanese with spaghetti</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image7"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 8, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Food</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Next Level Steak Sandwich</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image8"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 7, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Italian</a> <a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Quick Grilled Pizza</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <article class="blog-item blog-item-1col-list">
                     <div class="post-image">
                        <a href="blog-single-post.html">
                           <div class="list-image list-image9"></div>
                        </a>
                     </div>
                     <div class="post-holder ">
                        <ul class="post-meta">
                           <li class="meta-date">March 5, 2020</li>
                           <li class="meta-categ"><a href="#" rel="category tag">Recipes</a></li>
                        </ul>
                        <h2 class="article-title"><a href="blog-single-post.html">Corn-and-Ham Risotto</a></h2>
                        <div class="article-excerpt"> Maecenas ornare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et gravida. Maecenas id enim pharetra, sollicitudin dui eget, blandit lorem. Nunc vitae blandit lectus. Donec lacinia magna sit amet ...</div>
                        <a class="view-more" href="blog-single-post.html">Read More </a>
                     </div>
                  </article>
                  <div class="prev-next"><span class="nav-page"></span><span class="page-numbers current-page">1</span> <a class="page-numbers" href="#" title="Page 2">2</a> <span class="nav-page"><a href="#">Older &rarr;</a></span></div>
               </section>
            </div>
            <!-- /col-md-9 --> 
            <div class="col-md-3">
               <aside>
                  <ul>
                     <li id="categories-2" class="widget widget_categories">
                        <h5 class="widgettitle"><span>Categorias</span></h5>
                        <ul>
                           <li class="cat-item cat-item-2"><a href="#">Postres</a></li>
                           <li class="cat-item cat-item-3"><a href="#">Comida</a></li>
                           <li class="cat-item cat-item-4"><a href="#">Italiana</a></li>
                           <li class="cat-item cat-item-6"><a href="#">Recipientes</a></li>
                        </ul>
                     </li>
                     <li id="search-2" class="widget widget_search">
                        <form method="get" id="search-form" action="#"> <span><input name="s" class="search-string" placeholder="type and hit enter" type="text"></span></form>
                     </li>
                     <li id="recent-posts-2" class="widget widget_recent_entries">
                        <h5 class="widgettitle"><span>Recent Posts</span></h5>
                        <ul>
                           <li> <a href="#">Chef New Summer Dish</a></li>
                           <li> <a href="#">Realm of the Spirit Whisky</a></li>
                           <li> <a href="#">Restaurant Signature Dinner</a></li>
                        </ul>
                     </li>
                     <li id="archives-2" class="widget widget_archive">
                        <h5 class="widgettitle"><span>Archives</span></h5>
                        <ul>
                           <li><a href="#">March 2020</a></li>
                           <li><a href="#">March 2020</a></li>
                           <li><a href="#">March 2020</a></li>
                        </ul>
                     </li>
                     <li id="tag_cloud-2" class="widget widget_tag_cloud">
                        <h5 class="widgettitle"><span>Tags</span></h5>
                        <div class="tagcloud"><a href="#" class="tag-cloud-link tag-link-34 tag-link-position-1" >chef</a> <a href="#" class="tag-cloud-link tag-link-8 tag-link-position-2" >chicken</a> <a href="#" class="tag-cloud-link tag-link-28 tag-link-position-3" >chocolate</a> <a href="#" class="tag-cloud-link tag-link-30 tag-link-position-4" >cocktail</a> <a href="#" class="tag-cloud-link tag-link-9 tag-link-position-5" >corn</a> <a href="#" class="tag-cloud-link tag-link-35 tag-link-position-6" >dish</a> <a href="#" class="tag-cloud-link tag-link-31 tag-link-position-7" >drink</a> <a href="#" class="tag-cloud-link tag-link-11 tag-link-position-8" >egg</a> <a href="#" class="tag-cloud-link tag-link-13 tag-link-position-9" >mozzarella</a> <a href="#" class="tag-cloud-link tag-link-14 tag-link-position-10" >muffins</a> <a href="#" class="tag-cloud-link tag-link-15 tag-link-position-11" >pizza</a> <a href="#" class="tag-cloud-link tag-link-16 tag-link-position-12" >risotto</a> <a href="#" class="tag-cloud-link tag-link-17 tag-link-position-13" >salad</a> <a href="#" class="tag-cloud-link tag-link-18 tag-link-position-14" >spaghetti</a> <a href="#" class="tag-cloud-link tag-link-29 tag-link-position-15" >steak</a> <a href="#" class="tag-cloud-link tag-link-33 tag-link-position-16" >summer</a> <a href="#" class="tag-cloud-link tag-link-32 tag-link-position-17" >whisky</a></div>
                     </li>
                  </ul>
               </aside>
            </div>
            <!-- /col-md-3 --> 
         </div>
         <!-- /row --> 
      </div>
      <!-- /container --> 
      <!-- /WRAP CONTENT -->
      <!-- FOOTER -->
      <footer>
         <div class="container">
            <div class="footer-widgets">
               <div class="row">
                  <!-- FOOTER COLUMN 1 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-2" class="widget widget-footer widget_text">
                           <div class="textwidget">
                              <p><img class="size-full wp-image-665" src="assets/images/caverta-logo2.png" alt="" width="143" height="51"></p>
                              <p>For a truly memorable dining experience reserve in advance a table as soon as you can. Come and taste our remarkable food and wine.</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 2 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-3" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Dirección</span></h5>
                           <div class="textwidget">
                              <p>Calle 20<br>
                                 Cancún, Quintana Roo CP: 77535
                              </p>
                              <p>Cel: 9981801087<br>
                                 Correo: cassandratello25@gmail.com
                              </p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 3 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-4" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Horarios</span></h5>
                           <div class="textwidget">
                              <p>Lunes – Domingo<br>
                                 Almuerzo: 12PM – 2PM<br>
                                 Cena: 6PM – 10PM
                              </p>
                              <p>Hora Feliz: 4PM – 6PM</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 4 -->
                  <div class="col-md-3">
                     <div class="foo-block foo-last">
                        <div id="text-5" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Más información</span></h5>
                           <div class="textwidget">
                              <ul>
                                <li><a href="#">Degustaciones</a></li>
                                <li><a href="#">Anuncios</a></li>
                                <li><a href="#">Políticas de privacidad</a></li>
                            </ul>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
               </div>
               <!--row-->
            </div>
            <!-- footer-widgets -->
            <div class="copyright">
               <!-- COPYRIGHT -->
               <div class="footer-copy">
                  <p>Copyright © 2021, Caverta-Banquetes Gourmet . Designed by Puesyo</p>
               </div>
               <!-- SOCIAL ICONS -->
               <ul class="footer-social">
                  <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
               <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
               <li><a class="social-gplus" href="#" target="_blank"><i class="fab fa-google-plus"></i></a></li>
               <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
               <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
               <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
               </ul>
            </div>
            <!--copyright-->
         </div>
         <!--container-->
      </footer>
      <!-- /FOOTER -->
      <div class="scrollup">
         <a class="scrolltop" href="#">
         <i class="fa fa-chevron-up"></i>
         </a>
      </div>
      <!-- JS --> 
      <script src='assets/js/jquery.js'></script>
      <script src='assets/js/jquery-migrate.min.js'></script>
      <script src='assets/css/bootstrap/js/popper.min.js'></script>
      <script src='assets/css/bootstrap/js/bootstrap.min.js'></script>
      <script src='assets/js/jquery.easing.min.js'></script>
      <script src='assets/js/jquery.fitvids.js'></script>
      <script src='assets/js/owl-carousel/owl.carousel.min.js'></script>
      <script src='assets/js/jquery.magnific-popup.min.js'></script>
      <!-- MAIN JS -->
      <script src='assets/js/init.js'></script>
   </body>
</html>