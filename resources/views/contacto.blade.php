 <html lang="es">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Contacto</title>
      <!-- Google Fonts -->
      <link rel='stylesheet' id='caverta-font-css'  href='http://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700%7CLora:400,700,400i,700i' type='text/css' media='all' />
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' id='bootstrap-css'  href='assets/css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
      <!-- Font Awesome Icons CSS -->
      <link rel='stylesheet' id='font-awesome'  href='assets/css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
      <!-- Owl Carousel -->
      <link rel='stylesheet' id='owl-carousel'  href='assets/js/owl-carousel/owl.carousel.min.css' type='text/css' media='all' />
      <!-- Main CSS File -->
      <link rel='stylesheet' id='caverta-style-css'  href='style.css' type='text/css' media='all' />
      <!-- favicons -->
      <link rel="icon" href="assets/images/icons/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" href="assets/images/icons/favicon-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="assets/images/icons/favicon-180x180.png" />
   </head>
   <body class="home">
      <div class="menu-mask"></div>
      <!-- MOBILE MENU HOLDER -->
      <div class="mobile-menu-holder">
         <div class="modal-menu-container">
            <div class="exit-mobile">
               <span class="icon-bar1"></span>
               <span class="icon-bar2"></span>
            </div>
            <!-- MOBILE MENU -->
            <ul class="menu-mobile">
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contenido'); ?>">Blog</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					</li>
            </ul>
            <!-- /MOBILE MENU -->
         </div>
         <!-- modal-menu-container -->
         <div class="menu-contact"><div class="mobile-btn"> <a href="#" class="view-more">Book a Table</a></div>
            <ul class="mobile-contact">
               <li class="mobile-address">Calle 20<br />
                  Cancún, Quintana Roo CP: 77535
               </li>
               <li class="mobile-phone">9981801087</li>
               <li class="mobile-email">cassandratello25@gmail.com</li>
            </ul>
            <ul class="social-media">
               <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
            </ul>
         </div>
         <!-- /menu-contact-->
      </div>
      <!-- /MOBILE MENU HOLDER -->
      <!-- HEADER -->
      <header id="header-1" class="headerHolder header-1">
         <div class="nav-button-holder">
            <button type="button" class="nav-button">
            <span class="icon-bar"></span>
            </button>
         </div>
         <!-- /nav-button-holder-->
         <!-- LOGO -->
         <div class="logo logo-1"><a href="<?= url('/inicio'); ?>"><img class="img-fluid" src="assets/images/caverta-logo.png" alt="Caverta" /></a></div>
         <!-- MENU -->
         <nav class="nav-holder nav-holder-1">
            <ul class="menu-nav menu-nav-1">
              <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contenido'); ?>">Blog</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					
					</li>
            </ul>
         </nav>
         <!-- /MENU --> 
         <div class="btn-header">
            <p><a href="<?= url('/reservaciones'); ?>" class="view-more">Reservar</a></p>
         </div>
      </header>
      <!-- /HEADER -->
      <!-- TOP IMAGE -->
      <section class="topSingleBkg topPageBkg topPageCustomH">
         <div class="item-img top-contact"></div>
         <div class="inner-desc">
            <h1 class="post-title single-post-title">Contáctanos</h1>
            <span class="post-subtitle"> We're easy to get in touch with</span>
         </div>
      </section>
      <!-- /TOP IMAGE --> 
      <!-- WRAP CONTENT -->
      <div id="wrap-content" class="page-content custom-page-template">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="page-holder custom-page-template">
                     <div class="row">
                        <div class="col-md-4 alignc margin-b72">
                           <div class="contact-icon margin-b24"> <i class="far fa-map"></i></div>
                           <div class="widgettitle margin-b16">Ubicación</div>
                           <p>Calle 20<br> Cancún, Quintana Roo CP:77535</p>
                        </div>
                        <!-- /col-md-4 --> 
                        <div class="col-md-4 alignc margin-b72">
                           <div class="contact-icon margin-b24"> <i class="fas fa-phone"></i></div>
                           <div class="widgettitle margin-b16">Contacto</div>
                           <p>9981801087<br> cassandratello25@gmail.com</p>
                        </div>
                        <!-- /col-md-4 --> 
                        <div class="col-md-4 alignc margin-b72">
                           <div class="contact-icon margin-b24"> <i class="far fa-bell"></i></div>
                           <div class="widgettitle margin-b16">Horarios</div>
                           <p>Almuerzo: 12PM – 2PM<br> Cena: 6PM – 10PM</p>
                        </div>
                        <!-- /col-md-4 --> 
                     </div>
                     <!-- /row --> 
                     <div class="row">
                        <div class="col-md-6 mobile-margin-b72">
                           <div id="contact-holder-home">
                              <form method="post" id="contact-form-home" action='include/contact-process-home.php'>
                                 <div class="contact-holder">
                                    <input name="name" value="" size="40" class="comm-field" aria-required="true" aria-invalid="false" placeholder="Name" type="text">
                                    <input name="email" value="" size="40" class="comm-field" aria-required="true" aria-invalid="false" placeholder="Email" type="email">
                                    <input name="phone" value="" size="40" class="comm-field" aria-invalid="false" placeholder="Phone" type="text">
                                 </div>
                                 <!-- /contact-holder -->
                                 <p><textarea name="message" cols="40" rows="5" id="msg-contact" aria-required="true" aria-invalid="false" placeholder="Your Message"></textarea></p>
                                 <p class="antispam">Leave this empty: <input type="text" name="url" /></p>
                                 <p><input value="Book Now" id="submit-contact" type="submit"></p>
                              </form>
                           </div>
                           <div id="output-contact"></div>
                        </div>
                        <!-- /col-md-6 --> 
                        <div class="col-md-6">
                           <div class="gmaps">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d48406.76415912992!2d-73.91663!3d40.686686!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25c12a447e187%3A0x51a1c2db3c1d2403!2s58+Ralph+Ave%2C+Brooklyn%2C+NY+11221!5e0!3m2!1sen!2sus!4v1530037320308" height="300"></iframe>
                           </div>
                        </div>
                        <!-- /col-md-6 --> 
                     </div>
                     <!-- /row --> 
                  </div>
                  <!-- /custom-page-template -->
               </div>
               <!-- /col-md-12 --> 
            </div>
            <!-- /row --> 
         </div>
         <!-- /container --> 
      </div>
      <!-- /WRAP CONTENT -->
      <!-- FOOTER -->
      <footer>
         <div class="container">
            <div class="footer-widgets">
               <div class="row">
                  <!-- FOOTER COLUMN 1 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-2" class="widget widget-footer widget_text">
                           <div class="textwidget">
                              <p><img class="size-full wp-image-665" src="assets/images/caverta-logo2.png" alt="" width="143" height="51"></p>
                              <p>Para una experiencia gastronómica verdaderamente memorable, la comida y el ambiente se combinan tan cuidadosamente como la comida y el vino.</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 2 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-3" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Dirección</span></h5>
                           <div class="textwidget">
                            <p>Calle 20<br>
                                Cancún, Quintana Roo CP: 77535
                             </p>
                             <p>Cel: 9981801087<br>
                                Correo: cassandratello25@gmal.com
                            </p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 3 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-4" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Horarios</span></h5>
                           <div class="textwidget">
                            <p>Lunes – Domingo<br>
                                Almuerzo: 12PM – 2PM<br>
                                Cena: 6PM – 10PM
                             </p>
                             <p>Hora Feliz: 4PM – 6PM</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 4 -->
                  <div class="col-md-3">
                     <div class="foo-block foo-last">
                        <div id="text-5" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Más información</span></h5>
                           <div class="textwidget">
                              <ul>
                                <li><a href="#">Recomendaciones</a></li>
                                <li><a href="#">Actividades recreativas</a></li>
                                <li><a href="#">Política de privacidad</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
               </div>
               <!--row-->
            </div>
            <!-- footer-widgets -->
            <div class="copyright">
               <!-- COPYRIGHT -->
               <div class="footer-copy">
                  <p>Copyright © 2021, Banquetes Gourmet . Designed by puesyo</p>
               </div>
               <!-- SOCIAL ICONS -->
               <ul class="footer-social">
                  <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
               </ul>
            </div>
            <!--copyright-->
         </div>
         <!--container-->
      </footer>
      <!-- /FOOTER -->
      <div class="scrollup">
         <a class="scrolltop" href="#">
         <i class="fa fa-chevron-up"></i>
         </a>
      </div>
      <!-- JS --> 
      <script src='assets/js/jquery.js'></script>
      <script src='assets/js/jquery-migrate.min.js'></script>
      <script src='assets/css/bootstrap/js/popper.min.js'></script>
      <script src='assets/css/bootstrap/js/bootstrap.min.js'></script>
      <script src='assets/js/jquery.easing.min.js'></script>
      <script src='assets/js/jquery.fitvids.js'></script>
      <script src='assets/js/owl-carousel/owl.carousel.min.js'></script>
      <script src='assets/js/jquery.magnific-popup.min.js'></script>
      <!-- MAIN JS -->
      <script src='assets/js/init.js'></script>
      <!-- CONTACT FORM JS -->
      <script src='assets/js/jquery.form.min.js'></script>
      <script src='assets/js/contactform-home.js'></script>
   </body>
</html>
