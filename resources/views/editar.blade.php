<html lang="es">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Bolsa de Trabajo</title>
      <!-- Google Fonts -->
      <link rel='stylesheet' id='caverta-font-css'  href='http://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700%7CLora:400,700,400i,700i' type='text/css' media='all' />
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' id='bootstrap-css'  href="{{URL::asset('assets/css/bootstrap/css/bootstrap.min.css')}}" type='text/css' media='all' />
      <!-- Font Awesome Icons CSS -->
      <link rel='stylesheet' id='font-awesome'  href="{{URL::asset('assets/css/fontawesome/css/font-awesome.min.css')}}" type='text/css' media='all' />
      <!-- Owl Carousel -->
      <link rel='stylesheet' id='owl-carousel'  href="{{URL::asset('assets/js/owl-carousel/owl.carousel.min.css')}}" type='text/css' media='all' />
      <!-- Main CSS File -->
      <link rel='stylesheet' id='caverta-style-css'  href="{{URL::asset('style.css')}}" type='text/css' media='all' />
      <!-- favicons -->
      <link rel="icon" href="{{URL::asset('assets/images/icons/favicon-32x32.png')}}" sizes="32x32" />
      <link rel="icon" href="{{URL::asset('assets/images/icons/favicon-192x192.png')}}" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="{{URL::asset('assets/images/icons/favicon-180x180.png')}}" />
   </head>
   <body class="home">
      <div class="menu-mask"></div>
      <!-- MOBILE MENU HOLDER -->
      <div class="mobile-menu-holder">
         <div class="modal-menu-container">
            <div class="exit-mobile">
               <span class="icon-bar1"></span>
               <span class="icon-bar2"></span>
            </div>
            <!-- MOBILE MENU -->
            <ul class="menu-mobile">
					<li class="menu-item menu-item-has-children">
						<a href="home">Inicio</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="blog">Blog</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="team">Nuestro Equipo</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="galeria">Galería</a>
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="reservaciones">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="contacto">Contacto</a>
					</li>
            </ul>
            <!-- /MOBILE MENU -->
         </div>
         <!-- modal-menu-container -->
         <div class="menu-contact"><div class="mobile-btn"> <a href="#" class="view-more">Reservar</a></div>
            <ul class="mobile-contact">
               <li class="mobile-address">Calle 20<br />
                  Cancún, Quintana Roo CP: 77535
               </li>
               <li class="mobile-phone">9981801087</li>
               <li class="mobile-email">cassandratello25@gmail.com</li>
            </ul>
            <ul class="social-media">
               <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
            </ul>
         </div>
         <!-- /menu-contact-->
      </div>
      <!-- /MOBILE MENU HOLDER -->
      <!-- HEADER -->
      <header id="header-1" class="headerHolder header-1">
         <div class="nav-button-holder">
            <button type="button" class="nav-button">
            <span class="icon-bar"></span>
            </button>
         </div>
         <!-- /nav-button-holder-->
         <!-- LOGO -->
         <div class="logo logo-1"><a href="home"><img class="img-fluid" src="{{URL::asset('assets/images/caverta-logo.png')}}" alt="Caverta" /></a></div>
         <!-- MENU -->
         <nav class="nav-holder nav-holder-1">
            <ul class="menu-nav menu-nav-1">
              <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contenido'); ?>">Blog</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/nosotros'); ?>">Nuestro Equipo</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					
					</li>
            </ul>
         </nav>
         <!-- /MENU --> 
         <div class="btn-header">
            <p><a href="<?= url('/reservaciones'); ?>" class="view-more">Reservar</a></p>
         </div>
      </header>
      <!-- /HEADER -->
      <!-- TOP IMAGE -->
      <section class="topSingleBkg topPageBkg topPageCustomH">
         <div class="item-img top-contact"></div>
         <div class="inner-desc">
            <h1 class="post-title single-post-title">Permítenos Ayudarte</h1>
            <span class="post-subtitle"> Deja tus Datos y/o Curriculum</span>
         </div>
      </section>
      <!-- /TOP IMAGE --> 
      <!-- /WRAP CONTENT -->
      <body class="home">     
         <div id="wrap-content" class="page-content custom-page-template">
                <div class="container">
                   <div class="row">
                       <div class="col-md-12">
                           <div id="reservation-holder">
                              <form action="{{ url('curriculums.update', $curriculum->id) }}" method="POST" enctype="multipart/form-data">
                                 @csrf
                                 @method('put')
                                   <div class="contact-holder">
                                        <div class="row">
                                            <div clas="col-md-4">
                                               <label> Llena el Siguiente Formulario:</label>
                                            </div>
                                        </div> 
                                       <div class="row">
                                            <div class="col-md-4">
                                               <label> Nombre</label>
                                                <input name="nombre" value="{{old('nombre', $curriculum->nombre)}}" size="40" class="comm-field" @error('nombre') is-valid @enderror placeholder="Ingrese su(s) Nombre(s):" type="text">
                                               @error ('nombre')
                                               <p><i>{{ $message }}</i></p>
                                               @enderror 
                                            </div>
                                            <div class="col-md-4">
                                                <label> Apellidos</label>
                                                <input name="apellido" value="{{old('apellido', $curriculum->apellido)}}" size="40" class="comm-field"  placeholder="Ingrese sus apellidos:" type="text">
                                                @error ('apellido')
                                                <p><i>{{ $message }}</i></p>
                                                @enderror 
                                            </div>
                                            <div class="col-md-4">
                                              <label> Edad</label>
                                              <input name="edad" value="{{old('edad', $curriculum->edad)}}" size="40" class="comm-field"  placeholder="Ingrese su edad:" type="text">
                                            </div>
                                        </div>
                                        <!-- /row -->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label> Cargo</label>
                                                <input name="cargo" value="{{old('cargo', $curriculum->cargo)}}" size="40" class="comm-field" @error('cargo') is-valid @enderror placeholder="Ingrese cargo:" type="text">
                                                @error ('cargo')
                                                <p><i>{{ $message }}</i></p>
                                                @enderror
                                            </div>
                                            <div class="col-md-4">
                                                <label> Imagen</label>
                                                <input name="imagen" value="{{old('imagen', $curriculum->imagen)}}" size="40" class="comm-field" @error('imagen') is-valid @enderror placeholder="Suba su foto:"  type="file">
                                                @error ('imagen')
                                                <p><i>{{ $message }}</i></p>
                                                @enderror
                                            </div>
                                            <div clas="col-md-4">
                                                <label> Para comunicarnos con usted, deje su e-mail</label>
                                                <input name="email" value="{{old('email', $curriculum->email)}}" size="40" class="comm-field" @error('email') is-valid @enderror placeholder="Ingrese email:" type="email">
                                                @error ('email')
                                                <p><i>{{ $message }}</i></p>
                                                @enderror
                                            </div>
                                        </div>
                                          <p><input value="Enviar" id="submit-contact" type="submit"></p>
                                         <!-- /row -->
                                       </div>
                                     <!-- /contact-holder --> 
                                   </form>
                          </div>
                          <!-- /reservation-holder -->
                           <div id="output-contact"></div>
                       </div>
                  </div>
             </div>
          </div>
      </body>
      <!-- /WRAP CONTENT -->
      <!-- FOOTER -->
      <footer>
         <div class="container">
            <div class="footer-widgets">
               <div class="row">
                  <!-- FOOTER COLUMN 1 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-2" class="widget widget-footer widget_text">
                           <div class="textwidget">
                              <p><img class="size-full wp-image-665" src="{{URL::asset('assets/images/caverta-logo2.png')}}" alt="" width="143" height="51"></p>
                              <p>Para una experiencia gastronómica verdaderamente memorable, la comida y el ambiente se combinan tan cuidadosamente como la comida y el vino.</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 2 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-3" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Dirección</span></h5>
                           <div class="textwidget">
                            <p>Calle 20<br>
                                Cancún, Quintana Roo CP: 77535
                             </p>
                             <p>Cel: 9981801087<br>
                                Correo: cassandratello25@gmail.com
                            </p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 3 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-4" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Horarios</span></h5>
                           <div class="textwidget">
                            <p>Lunes – Domingo<br>
                                Almuerzo: 12PM – 2PM<br>
                                Cena: 6PM – 10PM
                             </p>
                             <p>Hora Feliz: 4PM – 6PM</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 4 -->
                  <div class="col-md-3">
                     <div class="foo-block foo-last">
                        <div id="text-5" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Más información</span></h5>
                           <div class="textwidget">
                              <ul>
                                <li><a href="#">Recomendaciones</a></li>
                                <li><a href="#">Actividades recreativas</a></li>
                                <li><a href="#">Política de privacidad</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
               </div>
               <!--row-->
            </div>
            <!-- footer-widgets -->
            <div class="copyright">
               <!-- COPYRIGHT -->
               <div class="footer-copy">
                  <p>Copyright © 2021, Banquetes Gourmet . Designed by Cass</p>
               </div>
               <!-- SOCIAL ICONS -->
               <ul class="footer-social">
                  <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
               </ul>
            </div>
            <!--copyright-->
         </div>
         <!--container-->
      </footer>
      <!-- /FOOTER -->
      <div class="scrollup">
         <a class="scrolltop" href="#">
         <i class="fa fa-chevron-up"></i>
         </a>
      </div>
      <!-- JS --> 
      <script src="{{URL::asset('assets/js/jquery.js')}}"></script>
      <script src="{{URL::asset('assets/js/jquery-migrate.min.js')}}"></script>
      <script src="{{URL::asset('assets/css/bootstrap/js/popper.min.js')}}"></script>
      <script src="{{URL::asset('assets/css/bootstrap/js/bootstrap.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/jquery.easing.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/jquery.fitvids.js')}}"></script>
      <script src="{{URL::asset('assets/js/owl-carousel/owl.carousel.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- MAIN JS -->
      <script src="{{URL::asset('assets/js/init.js')}}"></script>
      <!-- CONTACT FORM JS -->
      <script src="{{URL::asset('assets/js/jquery.form.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/contactform-home.js')}}"></script>
   </body>
</html>