<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Banquetes Gourmet</title>
      <!-- Google Fonts -->
      <link rel='stylesheet' id='caverta-font-css'  href='http://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700%7CLora:400,700,400i,700i' type='text/css' media='all' />
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' id='bootstrap-css'  href='assets/css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
      <!-- Font Awesome Icons CSS -->
      <link rel='stylesheet' id='font-awesome'  href='assets/css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
      <!-- Owl Carousel -->
      <link rel='stylesheet' id='owl-carousel'  href='assets/js/owl-carousel/owl.carousel.min.css' type='text/css' media='all' />
      <!-- Main CSS File -->
      <link rel='stylesheet' id='caverta-style-css'  href='style.css' type='text/css' media='all' />
      <!-- favicons -->
      <link rel="icon" href="assets/images/icons/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" href="assets/images/icons/favicon-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="assets/images/icons/favicon-180x180.png" />
   </head>
   <body class="home">
      <div class="menu-mask"></div>
      <!-- MOBILE MENU HOLDER -->
      <div class="mobile-menu-holder">
         <div class="modal-menu-container">
            <div class="exit-mobile">
               <span class="icon-bar1"></span>
               <span class="icon-bar2"></span>
            </div>
            <!-- MOBILE MENU -->
            <ul class="menu-mobile">
               <li class="menu-item menu-item-has-children current-menu-item">
						<a href="inicio">Inicio</a>

					</li>
					<li class="menu-item menu-item-has-children">
						<a href="contenido">Blog</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="nosotros">Nuestros Servicios</a>
	
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="galeria">Galería</a>

					</li>
               <li class="menu-item menu-item-has-children">
						<a href="reservaciones">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="contacto">Contacto</a>

					</li>
            </ul>
            <!-- /MOBILE MENU -->
         </div>
         <!-- modal-menu-container -->
         <div class="menu-contact"><div class="mobile-btn"> <a href="#" class="view-more">Reserva</a></div>
            <ul class="mobile-contact">
               <li class="mobile-address">Calle 20<br />
                  Cancún, Quintana Roo CP: 77535
               </li>
               <li class="mobile-phone">9981801087</li>
               <li class="mobile-email">cassandratello25.com</li>
            </ul>
            <ul class="social-media">
               <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
            </ul>
         </div>
         <!-- /menu-contact-->
      </div>
      <!-- /MOBILE MENU HOLDER -->
      <!-- HEADER -->
      <header id="header-1" class="headerHolder header-1">
         <div class="nav-button-holder">
            <button type="button" class="nav-button">
            <span class="icon-bar"></span>
            </button>
         </div>
         <!-- /nav-button-holder-->
         <!-- LOGO -->
         <div class="logo logo-1"><a href="inicio"><img class="img-fluid" src="assets/images/caverta-logo.png" alt="Caverta" /></a></div>
         <!-- MENU -->
         <nav class="nav-holder nav-holder-1">
            <ul class="menu-nav menu-nav-1">
               <li class="menu-item menu-item-has-children current-menu-item">
						<a href="inicio">Inicio</a>
					</li>
					<li class="menu-item menu-item-has-children">
                  <a href="contenido">Blog</a></li>
					</li>

					<li class="menu-item menu-item-has-children">
						<a href="nosotros">Nuestros Servicios</a>
	
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="galeria">Galería</a>
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="reservaciones">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="contacto">Contacto</a>
					</li>
            </ul>
         </nav>
         <!-- /MENU --> 
         <div class="btn-header">
            <p><a href="reservaciones" class="view-more">Reserva</a></p>
         </div>
      </header>
      <!-- /HEADER -->
      <!-- HOME SLIDER -->
      <div class="slider-container">
         <div class="owl-carousel owl-theme home-slider">
            <div class="slider-post slider-item-box-bkg">
               <div class="slider-img slide-1"></div>
               <div class="slider-caption">
                  <div class="slider-text">
                     <div class="intro-txt">Disfruta</div>
                     <h2>Banquetes Gourmet</h2>
                     <p>Ofrecemos una experiencia de menú de degustación altamente estacional y en continua evolución.<br />
                        <a class="slider-btn" href="contacto">Contáctanos</a>
                     </p>
                  </div>
               </div>
               <!--slider-caption-->
            </div>
            <!--slider-post-->
            <div class="slider-post slider-item-box-bkg">
               <div class="slider-img slide-2"></div>
               <div class="slider-caption">
                  <div class="slider-text">
                     <div class="intro-txt">Eventos Especiales</div>
                     <h2>Banquetes Gourmet</h2>
                     <p>Ofrecemos una experiencia de menú de degustación altamente estacional y en continua evolución.<br />
                        <a  class="slider-btn" href="contacto">Contáctanos</a>
                     </p>
                  </div>
               </div>
               <!--slider-caption-->
            </div>
            <!--slider-post-->
            <div class="slider-post slider-item-box-bkg">
               <div class="slider-img slide-3"></div>
               <div class="slider-caption">
                  <div class="slider-text">
                     <div class="intro-txt">Reuniones Privadas</div>
                     <h2>Banquetes Gourmet</h2>
                     <p>Ofrecemos una experiencia de menú de degustación altamente estacional y en continua evolución.<br />
                        <a  class="slider-btn" href="contacto">Contáctanos</a>
                     </p>
                  </div>
               </div>
               <!--slider-caption-->
            </div>
            <!--slider-post-->
         </div>
      </div>
      <!-- /HOME SLIDER --> 
      <!-- WRAP CONTENT -->
      <div id="wrap-content" class="page-content custom-page-template">
         <!-- SECTION 1 -->
         <div id="home-content-1" class="home-section home-section-1">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-md-3">
                     <img src="assets/images/home/welcome-1.jpg" alt="Welcome 1" />
                  </div>
                  <!-- /col-md-3 -->
                  <div class="col-md-6">
                     <div class="alignc">
                        <div class="smalltitle margin-b16">Bienvenidos</div>
                        <h2 class="home-title">Banquetes Gourmet</h2>
                        <p>Para una experiencia gastronómica verdaderamente memorable, la cocina y el ambiente se combinan tan cuidadosamente como la comida y el vino. Con el paso de los años, quien no ejercita, el distrito escolar. Porque no sé cómo perseguir el placer racionalmente encuentro consecuencias que son las penas de quien tiene. , Valor, es decir, de su labor y dolorosa</p>
                        <p><a class="view-more margin-t24" href="reservaciones">Reservar</a></p>
                     </div>
                  </div>
                  <!-- /col-md-6 -->
                  <div class="col-md-3">
                     <img src="assets/images/home/welcome-2.jpg" alt="Welcome 2" />
                  </div>
                  <!-- /col-md-3 -->
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 1 -->
         <!-- SECTION 2 -->
         <div id="home-content-2" class="home-section home-section-2 parallax">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="alignc">
                        <div class="smalltitle white margin-b16">Ven y Conócenos</div>
                        <h2>Creamos experiencias deliciosas</h2>
                     </div>
                  </div>
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 2 -->
         <!-- SECTION 3 -->
         <div id="home-content-3" class="home-section home-section-3">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="alignc">
                        <div class="smalltitle margin-b16">Nuestras especialidades</div>
                        <h2 class="home-title">Menú-Especialidades</h2>
                        <div class="width60">Explore texture, color and of course the ultimate tastes with our menu of the season. All the ingredients are fresh and carefully selected by our chefs. Enjoy an extraordinary dinning experience.</div>
                     </div>
                     <!-- MENU TAB -->
                     <ul class="nav nav-tabs menuTab" id="menuTab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" id="starters-tab" data-toggle="tab" href="#starters" role="tab" aria-controls="starters" aria-selected="true">STARTERS</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="main-course-tab" data-toggle="tab" href="#main-course" role="tab" aria-controls="main-course" aria-selected="false">MAIN COURSE</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="soups-tab" data-toggle="tab" href="#soups" role="tab" aria-controls="soups" aria-selected="false">SOUPS</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="desserts-tab" data-toggle="tab" href="#desserts" role="tab" aria-controls="desserts" aria-selected="false">DESSERTS</a>
                        </li>
                     </ul>
                     <div class="tab-content" id="menuTabContent">
                        <!-- STARTERS -->
                        <div class="tab-pane fade show active" id="starters" role="tabpanel" aria-labelledby="starters-tab">
                           <ul class="food-menu menu-2cols">
                              <li>
                                 <h4><span class="menu-title">Tomato Bruschetta</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Tomatoes, Olive Oil, Cheese</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Avocado &amp; Mango Salsa</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Avocado, Mango, Tomatoes</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Marinated Grilled Shrimp</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Fresh Shrimp, Oive Oil, Tomato Sauce</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Baked Potato Skins</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Potatoes, Oil, Garlic</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Maitake Mushroom</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Whipped Miso, Yaki Sauce, Sesame</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Lobster Picante</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Grilled Corn Elote, Queso Cotija, Chili</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Jambon Iberico</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Smoked Tomato Aioli, Idizabal Cheese, Spiced Pine Nuts</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Garlic Baked Cheese</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Finnish Squeaky Cheese, Eggplant Conserva, Black Pepper</div>
                              </li>
                           </ul>
                        </div>
                        <!-- MAIN COURSE -->
                        <div class="tab-pane fade" id="main-course" role="tabpanel" aria-labelledby="main-course-tab">
                           <ul class="food-menu menu-2cols">
                              <li>
                                 <h4><span class="menu-title">Braised Pork Chops</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">4 bone-in pork chops, olive oil, garlic, onion </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Coconut Fried Chicken </span><span class="menu-price"></span></h4>
                                 <div class="menu-text">8 chicken pieces, coconut milk, oil </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Chicken with Garlic &amp; Tomatoes </span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Chicken, cherry tomatoes, olive oil, dry white wine</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Prime Rib</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Rib, rosemary, black pepper, red wine </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Sriracha Beef Skewers</span><span class="menu-price">$</span></h4>
                                 <div class="menu-text">Beef, garlic, sesame oil, vinegar </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Crispy Tuna Fregola</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Fregola, Baby Arugula Roasted, Green Olives, Pine Nuts</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Charred Lamb Ribs</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Za’atar, Turkish BBQ, Sesame Yoghurt</div>
                              </li>
                           </ul>
                        </div>
                        <!-- SOUPS -->   
                        <div class="tab-pane fade" id="soups" role="tabpanel" aria-labelledby="soups-tab">
                           <ul class="food-menu menu-2cols">
                              <li>
                                 <h4><span class="menu-title">Terrific Turkey Chili</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Turkey, oregano, tomato paste, peppers </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Cream of Asparagus Soup</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Asparagus, potato, celery, onion, pepper  </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Creamy Chicken &amp; Wild Rice Soup</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Cooked chicken, salt, butter, heavy cream   </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Italian Sausage Tortellini</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Cheese tortellini, sausage, garlic, carrots, zucchini</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Italian Sausage Soup</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Italian sausage, garlic, carrots, zucchini </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Ham and Potato Soup</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Potatoes, ham, celery, onion, milk </div>
                              </li>
                           </ul>
                        </div>
                        <!-- DESSERTS --> 
                        <div class="tab-pane fade" id="desserts" role="tabpanel" aria-labelledby="desserts-tab">
                           <ul class="food-menu menu-2cols">
                              <li>
                                 <h4><span class="menu-title">Summer Berry and Coconut Tart</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Raspberries, blackberries, blueberries, graham cracker</div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Double Chocolate Cupcakes</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Chocolate, eggs, vanilla, milk  </div>
                              </li>
                              <li>
                                 <h4><span class="menu-title">Pumpkin Cookies Cream Cheese</span><span class="menu-price"></span></h4>
                                 <div class="menu-text">Pumpkin, sugar, butter, eggs   </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <!-- /MENU TAB -->
                  </div>
                  <!-- /col-md-12 -->
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 3 -->
         <!-- SECTION 4 -->
         <div id="home-content-4" class="home-section home-section-4 parallax">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="alignc">
                        <div class="smalltitle white margin-b16">Reserva ahora</div>
                        <h2>Cenas Privadas & Eventos Especiales</h2>
                     </div>
                  </div>
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 4 -->
         <!-- SECTION 5 -->
         <div id="home-content-5" class="home-section home-section-5">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <div class="alignc mobile-margin-b48">
                        <div class="smalltitle margin-b16">Conóce Nuestros servicios</div>
                        <h2 class="home-title">Banquetes</h2>
                        <p>Para una experiencia gastronómica verdaderamente memorable, la cocina y el ambiente se combinan tan cuidadosamente como la comida y el vino. Con el paso de los años, quien no ejercita, el distrito escolar. Porque no sé cómo perseguir el placer racionalmente encuentro consecuencias que son las penas de quien tiene. , Valor, es decir, de su labor y dolorosa.</p>
                        <p><a class="view-more margin-t24" href="nosotros">Conocer Más</a></p>
                     </div>
                  </div>
                  <!-- /col-md-6 -->
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-6 col-md-6">
                           <img src="assets/images/home/home-team-1.jpg" alt="Team 1" />
                        </div>
                        <!-- /col-md-3 -->
                        <div class="col-6 col-md-6">
                           <img src="assets/images/home/home-team-2.jpg" alt="Team 2" />
                        </div>
                        <!-- /col-md-3 -->
                     </div>
                     <!-- /row -->
                  </div>
                  <!-- /col-md-6 -->
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 5 -->
         <!-- SECTION 6 -->
         <div id="home-content-6" class="home-section home-section-6 parallax padding-tb108">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="alignc">
                        <div class="smalltitle white margin-b16">Comentarios</div>
                        <h2 class="home-title">Lo Que Nuestros Clientes Piensan...</h2>
                     </div>
                     <div class="owl-carousel owl-theme testimonial-slider">
                        <div class="testimonial-info">
                           <h4 class="testimonial-desc">"Cada detalle que tiene que se puede observar es muy bonito y visualmente relajante, me encanta como se ve todo, la decoración de las mesas, los emplatados son espectaculares, de los mejores que he visto"</h4>
                           <div>Juanito</div>
                           <span>El Juan</span>
                        </div>
                        <div class="testimonial-info">
                           <h4 class="testimonial-desc">"Realmente estoy muy satisfecha con el resultado que conociendo las dificultades que se tuvieron el personal pudo salir con todo resolviendo así imprevistos en el proceso"</h4>
                           <div>Angélica Avila</div>
                           <span>La Madrina</span>
                        </div>
                        <div class="testimonial-info">
                           <h4 class="testimonial-desc">"Es impresionante la imaginación y creatividad que pueden llegar a tener para transmitir el esfuerzo que se le dedica a cada cosa, sin duda estoy muy feliz de poder disfrutar de un rato agradable a lado de mi familia y de mis seres queridos"</h4>
                           <div>Paco Guitierrez</div>
                           <span>El Chato</span>
                        </div>
                        <div class="testimonial-info">
                           <h4 class="testimonial-desc">"Estoy impactado con el resultado final, realmente no me esperaba menos, son los mejores en el campo y los recomendaría sin nigún problema"</h4>
                           <div>Félix Torres</div>
                           <span>Los Torres</span>
                        </div>
                     </div>
                     <!-- /testimonial-slider -->
                  </div>
                  <!-- /col-md-12 -->
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 6 -->
         <!-- SECTION 7 -->
         <div id="home-content-7" class="home-section home-section-7">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">                     
                     <div class="alignc mobile-margin-b48">
                        <div class="smalltitle margin-b16">Regístrate</div>
                        <h2 class="home-title">Bolsa de Trabajo</h2>
                        <p>Para una experiencia gastronómica verdaderamente memorable, la cocina y el ambiente se combinan tan cuidadosamente como la comida y el vino. Con el paso de los años, quien no ejercita, el distrito escolar. Porque no sé cómo perseguir el placer racionalmente encuentro consecuencias que son las penas de quien tiene. , Valor, es decir, de su labor y dolorosa.</p>
                        <p><a class="view-more margin-t24" href="curriculums"> Más información</a></p>
                     </div>
                     <!-- /row -->
                     
                  </div>
                  <!-- /col-md-12 -->
               </div>
               <!-- /row -->
            </div>
            <!-- /container -->
         </div>
         <!-- /SECTION 7 -->
      </div>
      <!-- /WRAP CONTENT -->
      <!-- FOOTER -->
      <footer>
         <div class="container">
            <div class="footer-widgets">
               <div class="row">
                  <!-- FOOTER COLUMN 1 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-2" class="widget widget-footer widget_text">
                           <div class="textwidget">
                              <p><img class="size-full wp-image-665" src="assets/images/caverta-logo2.png" alt="" width="143" height="51"></p>
                              <p>
                                 Para una experiencia gastronómica verdaderamente memorable, la cocina y el ambiente se combinan tan cuidadosamente como la comida y el vino.</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 2 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-3" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Dirección</span></h5>
                           <div class="textwidget">
                              <p>Calle 20<br>
                                 Cancún, Quintana Roo CP: 77535
                              </p>
                              <p>Cel: 9981801087<br>
                                 Correo: cassandratello25@gmail.com
                              </p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 3 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-4" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Horario</span></h5>
                           <div class="textwidget">
                              <p>Lunes – Domingo<br>
                                 Almuerzo: 12PM – 2PM<br>
                                 Cena: 6PM – 10PM
                              </p>
                              <p>Hora Feliz: 4PM – 6PM</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 4 -->
                  <div class="col-md-3">
                     <div class="foo-block foo-last">
                        <div id="text-5" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Más información</span></h5>
                           <div class="textwidget">
                              <ul>
                                 <li><a href="#">Degustaciones</a></li>
                                 <li><a href="#">Anuncios</a></li>
                                 <li><a href="#">Políticas de privacidad</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
               </div>
               <!--row-->
            </div>
            <!-- footer-widgets -->
            <div class="copyright">
               <!-- COPYRIGHT -->
               <div class="footer-copy">
                  <p>Copyright © 2021, Banquetes Gourmet . Designed by Cass</p>
               </div>
               <!-- SOCIAL ICONS -->
               <ul class="footer-social">
                  <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
               </ul>
            </div>
            <!--copyright-->
         </div>
         <!--container-->
      </footer>
      <!-- /FOOTER -->
      <div class="scrollup">
         <a class="scrolltop" href="#">
         <i class="fa fa-chevron-up"></i>
         </a>
      </div>
      <!-- JS --> 
      <script src='assets/js/jquery.js'></script>
      <script src='assets/js/jquery-migrate.min.js'></script>
      <script src='assets/css/bootstrap/js/popper.min.js'></script>
      <script src='assets/css/bootstrap/js/bootstrap.min.js'></script>
      <script src='assets/js/jquery.easing.min.js'></script>
      <script src='assets/js/jquery.fitvids.js'></script>
      <script src='assets/js/owl-carousel/owl.carousel.min.js'></script>
      <script src='assets/js/jquery.magnific-popup.min.js'></script>
      <!-- MAIN JS -->
      <script src='assets/js/init.js'></script>
      <!-- CONTACT FORM JS -->
      <script src='assets/js/jquery.form.min.js'></script>
      <script src='assets/js/contactform-home.js'></script>
   </body>
</html>
