@extends('layouts.cont')

@section('contenido')
      <div id="wrap-content" class="page-content custom-page-template">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                  <div id="reservation-holder">
                    <div class="smalltitle margin-b16">Lista de Curriculums</div>
                    <a href="{{ route('curriculums.create')}}">Agregar Curriculum</a>
                    @if (session()->has('mensaje'))
                       <p style="color: red">{{session('mensaje')}}</p>
                    @endif
                    @foreach ($curriculums as $curriculum)
                       <table class="Taliniado">
                           <tr>
                              <th>Nombre</th> 
                              <p></p>
                              <th>Apellido</th>
                              <p></p>
                              <th>Edad</th>
                              <p></p>
                              <th>Cargo</th>
                              <p></p>
                              <th>Email</th>
                              <p></p>
                            </tr>
                            <tr>
                              <img src="../{{$curriculum->foto}}" alt="" >
                              <td><p>{{$curriculum->nombre}}</p></td>
                              <td><p>{{$curriculum->apellido}}</p></td>
                              <td><p>{{$curriculum->edad}}</p></td>
                              <td><p>{{$curriculum->cargo}}</p></td>
                              <td><p>{{$curriculum->email}}</p></td>
                              <td><a href="{{route('curriculums.show', $curriculum->id)}}">Info</a></td>
                              <td><a href="{{route('curriculums.edit', $curriculum->id)}}">Editar</a></td>
                              <td>
                                  <form action="{{route('curriculums.destroy',$curriculum->id)}}"method="post";>
                                       @csrf
                                       @method('Delete')
                                     <button type="submit">Eliminar</button>
                                     
                                    </form>
                              </td>
                              
                              
                           </tr>
                        
                        </table>

                     @endforeach
                   
                  </div>
              </div>
          </div>
       </div>
    </div> 

@endsection


