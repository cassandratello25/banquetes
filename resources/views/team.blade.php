<html lang="es">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Nuestros Servicios</title>
      <!-- Google Fonts -->
      <link rel='stylesheet' id='caverta-font-css'  href='http://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700%7CLora:400,700,400i,700i' type='text/css' media='all' />
      <!-- Bootstrap CSS -->
      <link rel='stylesheet' id='bootstrap-css'  href='assets/css/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
      <!-- Font Awesome Icons CSS -->
      <link rel='stylesheet' id='font-awesome'  href='assets/css/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
      <!-- Owl Carousel -->
      <link rel='stylesheet' id='owl-carousel'  href='assets/js/owl-carousel/owl.carousel.min.css' type='text/css' media='all' />
      <!-- Main CSS File -->
      <link rel='stylesheet' id='caverta-style-css'  href='style.css' type='text/css' media='all' />
      <!-- favicons -->
      <link rel="icon" href="assets/images/icons/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" href="assets/images/icons/favicon-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="assets/images/icons/favicon-180x180.png" />
   </head>
   <body class="home">
      <div class="menu-mask"></div>
      <!-- MOBILE MENU HOLDER -->
      <div class="mobile-menu-holder">
         <div class="modal-menu-container">
            <div class="exit-mobile">
               <span class="icon-bar1"></span>
               <span class="icon-bar2"></span>
            </div>
            <!-- MOBILE MENU -->
            <ul class="menu-mobile">
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contenido'); ?>">Blog</a>
						
					</li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
				
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
						
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					</li>
            </ul>
            <!-- /MOBILE MENU -->
         </div>
         <!-- modal-menu-container -->
         <div class="menu-contact"><div class="mobile-btn"> <a href="#" class="view-more">Book a Table</a></div>
            <ul class="mobile-contact">
               <li class="mobile-address">Calle 20<br />
                  Cancún, Quintana Roo CP: 77535
               </li>
               <li class="mobile-phone">9981801087</li>
               <li class="mobile-email">cassandratello25@gmail.com</li>
            </ul>
            <ul class="social-media">
               <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
            </ul>
         </div>
         <!-- /menu-contact-->
      </div>
      <!-- /MOBILE MENU HOLDER -->
      <!-- HEADER -->
      <header id="header-1" class="headerHolder header-1">
         <div class="nav-button-holder">
            <button type="button" class="nav-button">
            <span class="icon-bar"></span>
            </button>
         </div>
         <!-- /nav-button-holder-->
         <!-- LOGO -->
         <div class="logo logo-1"><a href="<?= url('/inicio'); ?>"><img class="img-fluid" src="assets/images/caverta-logo.png" alt="Caverta" /></a></div>
         <!-- MENU -->
         <nav class="nav-holder nav-holder-1">
            <ul class="menu-nav menu-nav-1">
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/inicio'); ?>">Inicio</a>
					
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contenido'); ?>">Blog</a>
						
					</li>
					<li class="menu-item menu-item-has-children current-menu-item">
						<a href="<?= url('/nosotros'); ?>">Nuestros Servicios</a>
				
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/galeria'); ?>">Galería</a>
						
					</li>
               <li class="menu-item menu-item-has-children">
						<a href="<?= url('/reservaciones'); ?>">Reservaciones</a>
					</li>
					<li class="menu-item menu-item-has-children">
						<a href="<?= url('/contacto'); ?>">Contacto</a>
					</li>
            </ul>
         </nav>
         <!-- /MENU --> 
         <div class="btn-header">
            <p><a href="<?= url('/reservaciones'); ?>" class="view-more">Reservar</a></p>
         </div>
      </header>
      <!-- /HEADER -->
      <!-- TOP IMAGE -->
      <section class="topSingleBkg topPageBkg">
         <div class="item-img top-team-4cols"></div>
         <div class="inner-desc">
            <h1 class="post-title single-post-title">Conoce Nuestros Servicios</h1>
            <span class="post-subtitle"> Amamos lo que hacemos</span>
         </div>
      </section>
      <!-- /TOP IMAGE --> 
      <!-- WRAP CONTENT -->
      <div id="wrap-content" class="page-content custom-page-template">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="page-holder custom-page-template">
                     <div class="width60 alignc margin-b72">
                        <h4>Somos un grupo diverso de diferentes culturas y orígenes que trabajan hacia el mismo objetivo. De modo que los tiempos de la caída no es trabajo, y una gran idea de algún tipo, nunca buscar placer.</h4>
                     </div>
                     <div class="row">
                        <div class="col-sm-6 col-md-3">
                           <div class="team-member">
                              <div class="team-img">
                                 <img src="assets/images/team/team-1.jpg" class="img-fluid" alt="">
                              </div>
                              <div class="team-desc">
                                 <h3 class="team-title">Clásico</h3>
                                 <div class="smalltitle">Banquete</div>
                                 <p>El clásico de los clásicos de los banquetes es el que se sirve en diferentes tiempos. Los invitados reciben la atención de los meseros en todo momento, por lo que es muy cómodo para que las personas charlen entre sí sin tener que moverse de sus asientos. Este tipo de banquete es considerado el más elegante.</p>
                              </div>
                           </div>
                        </div>
                        <!-- /col-md-3 --> 
                        <div class="col-sm-6 col-md-3">
                           <div class="team-member">
                              <div class="team-img">
                                 <img src="assets/images/team/team-2.jpg" class="img-fluid" alt="">
                              </div>
                              <div class="team-desc">
                                 <h3 class="team-title">Buffet</h3>
                                 <div class="smalltitle">Banquete</div>
                                 <p>El buffet puede ser más íntimo y divertido que un un banquete clásico, todo dependerá del tipo de celebración que estén preparando. Incluso, pueden romper con lo tradicional y optar por food trucks para bodas que, además de originales, les permitan ofrecer diferentes tipos de platillos.</p>
                              </div>
                           </div>
                        </div>
                        <!-- /col-md-3 --> 
                        <div class="col-sm-6 col-md-3">
                           <div class="team-member">
                              <div class="team-img">
                                 <img src="assets/images/team/team-3.jpg" class="img-fluid" alt="">
                              </div>
                              <div class="team-desc">
                                 <h3 class="team-title">Coctel</h3>
                                 <div class="smalltitle">Banquete</div>
                                 <p>Se determina a una reunión de poca duración, como máximo 4 horas, en el cual solo se sirven bebidas alcohólicas y refrescantes principalmente, y en ocasiones acompañadas de pasabocas</p>
                              </div>
                           </div>
                        </div>
                        <!-- /col-md-3 --> 
                        <div class="col-sm-6 col-md-3">
                           <div class="team-member">
                              <div class="team-img">
                                 <img src="assets/images/team/team-4.jpg" class="img-fluid" alt="">
                              </div>
                              <div class="team-desc">
                                 <h3 class="team-title">Menú tradicional</h3>
                                 <div class="smalltitle">Banquete</div>
                                 <p>En el que se sirve en diferentes tiempos. Un banquete en el que tanto invitados como novios comen o cenan sentados mientras un servicio de meseros sirve la comida. Es muy cómodo para que las personas conversen entre sí sin tener que moverse de sus asientos. Este tipo de banquete es considerado el más elegante.  </p>
                              </div>
                           </div>
                        </div>
                        <!-- /col-md-3 --> 
                     </div>
                     <!-- /row --> 
                     <div class="alignc">
                        <div class="smalltitle margin-none">Realiza tu reserva</div>
                        <h2>Somos tu mejor opción, te estamos esperando</h2>
                        <p><a class="view-more" href="<?= url('/reservaciones'); ?>">Reserva ahora</a></p>
                     </div>
                  </div>
                  <!-- /custom-page-template --> 
               </div>
               <!-- /col-md-12 --> 
            </div>
            <!-- /row --> 
         </div>
         <!-- /container --> 
      </div>
      <!-- /WRAP CONTENT -->
      <!-- FOOTER -->
      <footer>
         <div class="container">
            <div class="footer-widgets">
               <div class="row">
                  <!-- FOOTER COLUMN 1 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-2" class="widget widget-footer widget_text">
                           <div class="textwidget">
                              <p><img class="size-full wp-image-665" src="assets/images/caverta-logo2.png" alt="" width="143" height="51"></p>
                              <p>Para una experiencia gastronómica verdaderamente memorable, reserve con anticipación una mesa lo antes posible. Venga y pruebe nuestra excelente comida y vino.</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 2 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-3" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Dirección</span></h5>
                           <div class="textwidget">
                            <p>Calle 20<br>
                                Cancún, Quintana Roo CP:77535
                             </p>
                             <p>Cel: 9981801087<br>
                                Correo: cassandratello25@gmail.com
                             </p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 3 -->
                  <div class="col-md-3">
                     <div class="foo-block">
                        <div id="text-4" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Horarios</span></h5>
                           <div class="textwidget">
                              <p>Lunes – Domingo<br>
                                 Almuerzo: 12PM – 2PM<br>
                                 Cenna: 6PM – 10PM
                              </p>
                              <p>Hora Feliz: 4PM – 6PM</p>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
                  <!-- FOOTER COLUMN 4 -->
                  <div class="col-md-3">
                     <div class="foo-block foo-last">
                        <div id="text-5" class="widget widget-footer widget_text">
                           <h5 class="widgettitle"><span>Más información</span></h5>
                           <div class="textwidget">
                            <ul>
                                <li><a href="#">Degustaciones</a></li>
                                <li><a href="#">Anuncios</a></li>
                                <li><a href="#">Políticas de privacidad</a></li>
                            </ul>
                           </div>
                        </div>
                     </div>
                     <!--foo-block-->
                  </div>
                  <!--col-md-3-->
               </div>
               <!--row-->
            </div>
            <!-- footer-widgets -->
            <div class="copyright">
               <!-- COPYRIGHT -->
               <div class="footer-copy">
                  <p>Copyright © 2021, Banquetes Gourmet . Designed by Cass</p>
               </div>
               <!-- SOCIAL ICONS -->
               <ul class="footer-social">
                  <li><a class="social-facebook" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a class="social-twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                  <li><a class="social-tripadvisor" href="#" target="_blank"><i class="fab fa-tripadvisor"></i></a></li>
                  <li><a class="social-instagram" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                  <li><a class="social-pinterest" href="#" target="_blank"><i class="fab fa-pinterest"></i></a></li>
               </ul>
            </div>
            <!--copyright-->
         </div>
         <!--container-->
      </footer>
      <!-- /FOOTER -->
      <div class="scrollup">
         <a class="scrolltop" href="#">
         <i class="fa fa-chevron-up"></i>
         </a>
      </div>
      <!-- JS --> 
      <script src='assets/js/jquery.js'></script>
      <script src='assets/js/jquery-migrate.min.js'></script>
      <script src='assets/css/bootstrap/js/popper.min.js'></script>
      <script src='assets/css/bootstrap/js/bootstrap.min.js'></script>
      <script src='assets/js/jquery.easing.min.js'></script>
      <script src='assets/js/jquery.fitvids.js'></script>
      <script src='assets/js/owl-carousel/owl.carousel.min.js'></script>
      <script src='assets/js/jquery.magnific-popup.min.js'></script>
      <!-- MAIN JS -->
      <script src='assets/js/init.js'></script>
   </body>
</html>