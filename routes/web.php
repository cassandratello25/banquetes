<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CurriculumsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/inicio', function(){
    return view('home');
});

Route::get('/nosotros', function(){
    return view('team');
});

Route::get('/contacto', function(){
    return view('contacto');
});

Route::get('/contenido', function(){
    return view('blog');
});

Route::get('/galeria', function(){
    return view('galeria');
});

Route::get('/reservaciones', function(){
    return view('reservaciones');
});

Route::get('/lista_curriculums', function(){
  return view('lista_curriculums');
});
/*
Route::get('quierover', function(){
    return view('curriculums');
  });

Route::get('/prueba', function(){
    return view('formdos');
});

Route::get('/bolsa', function(){
    return view('registro_curriculum');
});

Route::get('registro_curriculums',[CurriculumsController::class,'create']);
Route::post('registro_curriculums',[CurriculumsController::class,'store']);
*/

Route::resource('curriculums','CurriculumsController');
/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/